import numpy as np
import matplotlib.pyplot as plt
import pickle
from hmmlearn import hmm
from sklearn.externals import joblib
from datetime import datetime
from collections import Counter

startTime = datetime.now()
vocabulary =[]

data =np.load("data.dat")

#a = np.hsplit(data, np.arange(1000, 56829, 1000))

 n = 10 * 50 # 10 speakers and 50 states per spekaer 


statess = []

model = hmm.GaussianHMM(n_components=n, covariance_type="diag")#,init_params = "")
model.transmat_ = np.ones((n, n)) / n
model.startprob_ = np.ones(n) / n
fit = model.fit(data.T)
z=fit.decode(data.T,algorithm = 'viterbi')[1]
statess.append(z)

print("done")

print(datetime.now()-startTime)


