function [w1, h1, cost]=nmf_l1(v1,wini1,hini1,niter,lambdasmooth)
%% NMF avec lissage L1
% v1 : donnees a decomposer, taille F*N
% wini1 : composantes initiales, taille F*K
% hini1 : activations initiales, taille K*N
% niter : nombre d'iterations avant arret
% lambdasmooth : ponderation accordee au lissage L1
% ===
% w1 : composantes finales, taille F*K
% h1 : activations finales, taille K*N
% cost : valeur de la fonction objectif au cours des iterations, taille niter*1

% Dimensions
n=size(hini1,2);
f=size(v1,1);

% Initialisation
scale_1=sum(wini1,1);
w1=wini1.*repmat(scale_1.^-1,f,1); % Telle qu'est organisee la boucle, on commence a travailler sur des donnees normalisees
h1=hini1.*repmat(scale_1',1,n);

if nargout>2
    keep_score=true;
    cost=zeros(niter,1);
    cost(1)=calc_cout(v1,w1,h1,lambdasmooth);
else
    keep_score=false;
end

for i=2:niter

    % Coeffs. lambda_k egaux a 1 pour les deux systemes, on part d'une version rescalee
    
    % Update h1
    ratio=v1./(w1*h1);
    ratio(isnan(ratio))=1; % Les NaN proviennent de 0/0, qui correspond a un cas parfaitement approxime donc ratio de 1
    psi_k_n=h1.*(w1'*ratio);
    
    for j=1:n % Non separabilite => boucle
        psi_k=psi_k_n(:,j);
        
        h_n_prec=h1(:,max(1,j-1));h_n_suiv=h1(:,min(n,j+1));
        h_min=min(h_n_prec,h_n_suiv);h_max=max(h_n_prec,h_n_suiv); % Points critiques ou il y a des sauts dans la derivee
        
        val_p1l=1-2*lambdasmooth-psi_k./h_min;val_p1r=val_p1l+2*lambdasmooth; % Valeurs de la derivee a gauche et droite de ces points
        val_p2l=1-psi_k./h_max;val_p2r=val_p2l+2*lambdasmooth;
        
        force_h_min=val_p1l<=0&val_p1r>=0; % Cas particuliers, la derivee change de signe sur un des points critiques
        force_h_max=val_p2l<=0&val_p2r>=0;
        
        h1(:,j)=psi_k./(1+2*lambdasmooth*((val_p2r<0)-(val_p1l>0))); % Expression generique du resultat, qui depend des signes aux points critiques
        h1(force_h_min,j)=h_min(force_h_min);h1(force_h_max,j)=h_max(force_h_max); % Les cas particuliers sont directement reintegres
    end

    % Update w1
    ratio=v1./(w1*h1);
    ratio(isnan(ratio))=1;
    phi_f_k=w1.*(ratio*h1');
    w1=bsxfun(@rdivide,phi_f_k,(sum(h1,2)'+lambdasmooth*sum(abs(h1(:,2:end)-h1(:,1:end-1)),2)'));
    
    % Renormalisation
    scale_1=sum(w1,1);
    w1=bsxfun(@times,w1,scale_1.^-1);
    h1=bsxfun(@times,h1,scale_1');
    
    % Stockage eventuel scores
    if keep_score
        cost(i)=calc_cout(v1,w1,h1,lambdasmooth);
    end
    
end

end

function cout=calc_cout(v1,w1,h1,lambdasmooth)
% Suboptimal puisqu'on ne reprend pas les calculs faits dans la boucle mais bien separe de la sorte

v_app1=w1*h1;
v_tmp1=v1.*log(v1./v_app1);
v_tmp1(isnan(v_tmp1))=0; % Ratio 0/0 => nan correspondant a un cas parfaitement approxime ou 0/0=1
v_tmp1=v_tmp1-v1+v_app1;

v_tmpj=abs(h1(:,2:end)-h1(:,1:end-1));

cout=sum(v_tmp1(:))+lambdasmooth*sum(v_tmpj(:));

end
