import numpy as np
import librosa
from collections import Counter
import numpy as np
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
from xarray import DataArray
import scipy.io as sio
from datetime import datetime
starttime = datetime.now()



def getPathToGroundtruth(episode):
    """Return path to groundtruth file for episode"""
    pathToGroundtruth = "../../../tbbt/TBBT/season01/Audio/" \
                        + "TheBigBangTheory.Season01.Episode%02d.en.wav" % episode
    return pathToGroundtruth

def getMFCC(episode):
    
    filename = getPathToGroundtruth(episode)
    
    y, sr = librosa.load(filename)  # Y gives 
    print(y,sr)
    
    data = librosa.feature.mfcc(y=y, sr=sr,)
    
    return data

def getduration(episode):
    
    pathToAudioFile = getPathToGroundtruth(episode)
    
    y, sr = librosa.load(pathToAudioFile)
    
    duration = librosa.get_duration(y=y, sr=sr)
    
    return duration

data = np.load("data.dat")
def  get_kmeans(data,n_states):
	k_means = KMeans(n_clusters=n_states,verbose =True, max_iter=300,n_init=1)

	k_means.fit(data.T)

	states = k_means.predict(data.T)
	return states 

def get_window(states,windowSize,step):
    Matrix=[]
    K= [states[i : i + windowSize] for i in range(0, len(states), step)]
    for i in K:
        if len(i) >= (windowSize-1):
            Matrix.append(i)
    return Matrix

'''def Vfinal(s_t,Matrix):
    state_no = np.arange(0,s_t)
    frame_no = np.arange(0,len(Matrix))                  
    V= DataArray(np.zeros(( len(state_no),len(Matrix) )), coords=[('States_count', state_no), ('Frame',frame_no)])
    for i,j in enumerate(Matrix):
        for k in j:
        	V.loc[k,i] +=1
    #k=np.finfo(float).eps
    #V[V==0] = k
    return V''' # One way of counting the elements 

def get_V_matrix(s_t,Matrix):
	E = np.finfo(float).eps
	V = np.zeros((s_t,len(Matrix)))
	for i,row in enumerate(Matrix):
		a = np.bincount(row)
		b=np.zeros(s_t)
		b[:len(a)]=a
		V[:,i]=b
	V[V==0] = E
	return V


n=500
states = get_kmeans(data,n)
print('states done')
Matrix = get_window(states,75,2)
print('Matrix done')
K      = get_V_matrix(n,Matrix)

sio.savemat('V_matrix.mat', {'V':K})

print(datetime.now()- starttime)


