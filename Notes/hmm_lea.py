import numpy as np
import matplotlib.pyplot as plt
import pickle
from hmmlearn import hmm
#import hmmlearn.hmm as hmm
import librosa
from datetime import datetime
from collections import Counter

startTime = datetime.now()
vocabulary =[]

def getPathToGroundtruth(episode):
    """Return path to groundtruth file for episode"""
    pathToGroundtruth = "../../../tbbt/TBBT/season01/Audio/" \
                        + "TheBigBangTheory.Season01.Episode%02d.en.wav" % episode
    return pathToGroundtruth
#getPathToGroundtruth(1)

def getduration(episode):
    pathToAudioFile = getPathToGroundtruth(episode)
    y, sr = librosa.load(pathToAudioFile)
    duration = librosa.get_duration(y=y, sr=sr)
    return duration
'''def getMFCC(episode):
    filename = getPathToGroundtruth(episode)
    y, sr = librosa.load(filename)  # Y gives 
    data = librosa.feature.mfcc(y=y, sr=sr)
    return data'''

#N = 100 #D = 20
'''def hmml(N,Datasize,data):
	mu = np.mean(data, axis=1)
	model = hmm.GaussianHMM(n_components=N, covariance_type="full")
	model.transmat_ = np.ones((N, N)) / N
	model.startprob_ = np.ones(N) / N
	remodel = hmm.GaussianHMM(n_components=N, covariance_type="full", )
	r=remodel.fit(data.T)
	z=r.decode(data.T,algorithm='viterbi')[1]
	count = Counter(z)
	return z'''


#data_m = getMFCC(1)
duration = getduration(1)
#data = data_m.dump("data.dat")
data =np.load("data.dat")
Datasize = len(data)
k = 11
N= k * 5
#States = hmml(N,Datasize,data)

frames            = data.shape[1]
duration 		  = getduration(1)
frames_per_second = frames / duration
window_size       = 1.5 

window_frame      = int(np.ceil (frames / (frames_per_second*window_size)))











print(datetime.now()-startTime)


