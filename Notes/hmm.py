import numpy as np
import librosa
import matplotlib.pyplot as plt
from datetime import datetime
from pomegranate import *
from pomegranate import HiddenMarkovModel as Model
from collections import Counter

startTime = datetime.now()
vocabulary =[]

def getPathToGroundtruth(episode):
    """Return path to groundtruth file for episode"""
    pathToGroundtruth = "../../../tbbt/TBBT/season01/Audio/" \
                        + "TheBigBangTheory.Season01.Episode%02d.en.wav" % episode
    return pathToGroundtruth
getPathToGroundtruth(1)

def getMFCC(episode):
    filename = getPathToGroundtruth(episode)
    y, sr = librosa.load(filename)
    data = librosa.feature.mfcc(y=y, sr=sr)
    return data
data = getMFCC(1)

N = 100
D = 20
#mean = [np.ones(D) for i in range(N)]
mean = [np.random.uniform(0,1,size=D) for i in range(N)]

def random_diagnal_matrix(G):
    a= np.zeros((G,G))
    for i in range(len(a)):
        a[i][i]= np.random.rand()
    return a

covariance = [random_diagnal_matrix(D) for i in range(N)]
distributions = [MultivariateGaussianDistribution(mean[i], covariance[i]) for i in range(N)]
transition = np.ones((N, N)) / N
starts = np.ones(N) / N
ends = np.ones(N) / N

hmm = HiddenMarkovModel.from_matrix(transition, distributions, starts, ends)

hmm.fit([data], algorithm='baum-welch')

mu = hmm.states[0].distribution.parameters[0]

cov = hmm.states[0].distribution.parameters[1]

#for i in range (0,25):

path = hmm.viterbi(data.T)[1]

x=[state for state, _ in path]

count = Counter(x)

vocabulary.append([count])

print(vocabulary)
print(datetime.now()-startTime)


